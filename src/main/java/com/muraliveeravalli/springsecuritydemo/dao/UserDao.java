package com.muraliveeravalli.springsecuritydemo.dao;

import com.muraliveeravalli.springsecuritydemo.entity.User;

public interface UserDao {

    User findByUserName(String userName);

    void save(User user);
}
