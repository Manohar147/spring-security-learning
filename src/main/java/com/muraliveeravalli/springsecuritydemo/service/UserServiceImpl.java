package com.muraliveeravalli.springsecuritydemo.service;

import com.muraliveeravalli.springsecuritydemo.dao.UserDao;
import com.muraliveeravalli.springsecuritydemo.entity.Role;
import com.muraliveeravalli.springsecuritydemo.entity.User;
import com.muraliveeravalli.springsecuritydemo.user.CrmUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;
    @Autowired
    private BCryptPasswordEncoder passwordEncoder;


    @Transactional
    @Override
    public User findByUserName(String userName) {
        return userDao.findByUserName(userName);
    }

    @Override
    @Transactional
    public void save(CrmUser crmUser) {
        User user = saveUser(crmUser);
// give user default role of "employee"
        user.setRoles(Arrays.asList(new Role("ROLE_EMPLOYEE")));
// save user in the database
        userDao.save(user);
    }

    private User saveUser(CrmUser crmUser) {
        User user = new User();
// assign user details to the user object
        user.setUserName(crmUser.getUserName());
        user.setPassword(passwordEncoder.encode(crmUser.getPassword()));
        user.setFirstName(crmUser.getFirstName());
        user.setLastName(crmUser.getLastName());
        user.setEmail(crmUser.getEmail());
        return user;
    }

    @Override
    @Transactional
    public void save(CrmUser crmUser, List<GrantedAuthority> authorities) {

        User user = saveUser(crmUser);
// give user default role of "employee"
        user.setRoles(authorities.stream().map(r -> new Role(r.getAuthority())).collect(Collectors.toList()));
// save user in the database
        userDao.save(user);
    }

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String userName) throws
            UsernameNotFoundException {
        User user = userDao.findByUserName(userName);
        if (user == null) {
            throw new UsernameNotFoundException("Invalid username or password.");
        }
        return new
                org.springframework.security.core.userdetails.User(user.getUserName(),
                user.getPassword(),
                mapRolesToAuthorities(user.getRoles()));
    }

    private Collection<? extends GrantedAuthority>
    mapRolesToAuthorities(Collection<Role> roles) {
        return roles.stream().map(role -> new
                SimpleGrantedAuthority(role.getName())).collect(Collectors.toList());
    }
}
