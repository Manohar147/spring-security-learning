package com.muraliveeravalli.springsecuritydemo.service;

import com.muraliveeravalli.springsecuritydemo.entity.User;
import com.muraliveeravalli.springsecuritydemo.user.CrmUser;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;

public interface UserService extends UserDetailsService {

    User findByUserName(String userName);

    void save(CrmUser crmUser);

     void save(CrmUser crmUser, List<GrantedAuthority> authorities);

}
