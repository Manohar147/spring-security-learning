<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%--
  Created by IntelliJ IDEA.
  User: sankeerthana
  Date: 09-11-2019
  Time: 12:28
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Home</title>
</head>
<body>
<h1>This is my homepage</h1>
<p>User id: <security:authentication property="principal.username"/></p>
<p>Role(s): <security:authentication property="principal.authorities"/></p>
<hr/>
<security:authorize access="hasRole('MANAGER')">
    <%--For Managers --%>
    <a href="${pageContext.request.contextPath}/leaders">Leaders</a>(Only for Managers)
</security:authorize>
<hr/>
<security:authorize access="hasRole('ADMIN')">
    <%--For Admin--%>
    <a href="${pageContext.request.contextPath}/systems">IT Systems Meeting</a>(only For Admin peeps)
</security:authorize>
<hr/>
<form:form action="${pageContext.request.contextPath}/logout" method="post">
    <input type="submit" value="logout">
</form:form>
</body>
</html>
